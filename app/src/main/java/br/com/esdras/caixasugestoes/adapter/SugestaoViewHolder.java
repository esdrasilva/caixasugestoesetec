package br.com.esdras.caixasugestoes.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.esdras.caixasugestoes.R;

public class SugestaoViewHolder extends RecyclerView.ViewHolder {

    ImageView imageView ;
    TextView textViewTipoSugestao;
    TextView textViewSugestao;
    TextView textViewDataHora;
    TextView textViewAluno;
    TextView textViewCurso;

    public SugestaoViewHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.imageViewTipo);
        textViewTipoSugestao = itemView.findViewById(R.id.textViewTipo);
        textViewSugestao = itemView.findViewById(R.id.textViewSugestao);
        textViewCurso = itemView.findViewById(R.id.textViewCurso);
        textViewAluno = itemView.findViewById(R.id.textViewNome);
        textViewDataHora = itemView.findViewById(R.id.textViewData);
    }
}
