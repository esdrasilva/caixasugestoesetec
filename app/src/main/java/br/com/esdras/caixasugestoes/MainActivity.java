package br.com.esdras.caixasugestoes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.textfield.TextInputLayout;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import br.com.esdras.caixasugestoes.model.Curso;


public class MainActivity extends AppCompatActivity {

    RequestQueue queue;
    TextInputLayout textInputLayoutSugestao, textInputLayoutNome;
    Spinner spinnerCurso, spinnerTipo;

    String urlAdd;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Curso> cursos = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        spinnerCurso = findViewById(R.id.spinnerCurso);
        spinnerTipo = findViewById(R.id.spinnerTipo);
        textInputLayoutSugestao = findViewById(R.id.textInputLayoutSugestao);
        textInputLayoutNome = findViewById(R.id.textInputLayoutNome);

        queue = Volley.newRequestQueue(this);

        String url = "https://ansuggestionbox.000webhostapp.com/cursos.php";
        urlAdd = "https://ansuggestionbox.000webhostapp.com/add.php";

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Carregando...");
        progressDialog.setMessage("Obtendo lista de cursos");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RESPONSE: ", response);
                try {
                    cursos.addAll(Arrays.asList(mapper.readValue(response, Curso[].class)));

                    List<String> cursosString = cursos.stream().map(Curso::getCurso).collect(Collectors.toList());

                    ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(),
                            R.layout.support_simple_spinner_dropdown_item, cursosString);

                    spinnerCurso.setAdapter(arrayAdapter);
                    progressDialog.dismiss();
                } catch (JsonProcessingException e) {
                    Log.e("Erro na requisição: ", Objects.requireNonNull(e.getLocalizedMessage()));
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Erro na requisição: ", Objects.requireNonNull(error.getLocalizedMessage()));
            }
        });

        Button button = findViewById(R.id.buttonEnviarSugestao);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarRequisicao();
            }
        });

        queue.add(stringRequest);
    }

    private void enviarRequisicao() {
        queue = Volley.newRequestQueue(this);
        progressDialog.setMessage("Inserindo dados...");
        progressDialog.show();
        StringRequest stringRequestPost = new StringRequest(Request.Method.POST, urlAdd, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> map = new HashMap<>();
                map.put("tipo_id", String.valueOf(spinnerTipo.getSelectedItemPosition()+1));
                map.put("curso_id", String.valueOf(spinnerCurso.getSelectedItemPosition() + 1));
                map.put("sugestao", textInputLayoutSugestao.getEditText().getText().toString());
                map.put("identificacao", textInputLayoutNome.getEditText().getText().toString());
                return map;
            }
        };
        queue.add(stringRequestPost);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.listarSugestoes){
            startActivity(new Intent(getApplication(), SugestoesActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}