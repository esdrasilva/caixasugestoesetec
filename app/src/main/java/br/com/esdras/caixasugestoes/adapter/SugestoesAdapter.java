package br.com.esdras.caixasugestoes.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import br.com.esdras.caixasugestoes.R;
import br.com.esdras.caixasugestoes.model.Sugestao;

public class SugestoesAdapter extends RecyclerView.Adapter<SugestaoViewHolder> {

    List<Sugestao> sugestoes = new ArrayList<>();

    public SugestoesAdapter(List<Sugestao> sugestoes) {
        this.sugestoes = sugestoes;
    }

    @NonNull
    @Override
    public SugestaoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview, parent, false);
        return new SugestaoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SugestaoViewHolder holder, int position) {
        Sugestao sugestao = sugestoes.get(position);
        holder.imageView.setImageResource(getImageResource(sugestao.getTipoSugestao()));
        holder.textViewSugestao.setText(sugestao.getSugestao());
        holder.textViewAluno.setText(sugestao.getIdentificacao());
        holder.textViewCurso.setText(sugestao.getCurso());
        holder.textViewDataHora.setText(LocalDateTime.parse(sugestao.getDatahora(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                .format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
    }

    @Override
    public int getItemCount() {
        return sugestoes.size();
    }

    private int getImageResource(String tipoSugestao) {
        int imdId = 0;
        switch (tipoSugestao) {
            case "Comentário":
                imdId = R.drawable.chat;
                break;
            case "Crítica":
                imdId = R.drawable.brokenheart;
                break;
            case "Elogio":
                imdId = R.drawable.like;
                break;
            case "Sugestão":
                imdId = R.drawable.idea;
                break;
        }
        return imdId;
    }
}
