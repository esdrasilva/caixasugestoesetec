package br.com.esdras.caixasugestoes.model;

import java.util.Objects;

public class Sugestao {
    private String id;
    private String sugestao;
    private String tipoSugestao;
    private String curso;
    private String identificacao;
    private String datahora;

    public Sugestao(String id, String sugestao, String tipoSugestao, String curso, String identificacao, String datahora) {
        this.id = id;
        this.sugestao = sugestao;
        this.tipoSugestao = tipoSugestao;
        this.curso = curso;
        this.identificacao = identificacao;
        this.datahora = datahora;
    }

    public Sugestao() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSugestao() {
        return sugestao;
    }

    public void setSugestao(String sugestao) {
        this.sugestao = sugestao;
    }

    public String getTipoSugestao() {
        return tipoSugestao;
    }

    public void setTipoSugestao(String tipoSugestao) {
        this.tipoSugestao = tipoSugestao;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }

    public String getDatahora() {
        return datahora;
    }

    public void setDatahora(String datahora) {
        this.datahora = datahora;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sugestao sugestao = (Sugestao) o;
        return Objects.equals(id, sugestao.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Sugestao{" +
                "id=" + id +
                ", sugestao='" + sugestao + '\'' +
                ", tipoSugestao='" + tipoSugestao + '\'' +
                ", curso='" + curso + '\'' +
                ", aluno='" + identificacao + '\'' +
                ", dataHora='" + datahora + '\'' +
                '}';
    }
}
